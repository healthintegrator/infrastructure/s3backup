# S3 Backup Utilities

This project provides a set of S3 backup utilities. The utilities are written as
[wrappers][wrapper] around the [MinIO] [client][mc] or the [s3cmd][s3cmd]. For
cases when SSE-C is not possible, the Docker image ships a version of [age]. The
implementation on top of [s3cmd] does not support SSE-C.

  [MinIO]: https://min.io/
  [mc]: https://docs.min.io/docs/minio-client-quickstart-guide.html
  [s3cmd]: https://github.com/s3tools/s3cmd
  [wrapper]: ./lib/s3.sh
  [age]: https://age-encryption.org/

## Scripts

The scripts available are as described below. They all provide command-line help
through the `--help` option (or `-h`):

* [ifcopy.sh](./ifcopy.sh) will copy recent files that have been placed in a
  source directory to a destination directory only if the latest file is
  different from the previous one in chronological order. When copying, siblings
  files (i.e. with the same basename but a different extension) can be copied
  along. The source or destination directories can be on s3 buckets as long as
  [mc] has access to them.
* [cleaner.sh](./cleaner.sh) will remove older files within a directory, keeping
  only a limited set of files. The directory can be at on a s3 bucket, as long
  as [mc] has access to them.
* [tar.sh](./tar.sh) will tar directories/files within a source directory and
  send a compressed tar file of the content to a destination directory. The
  destination can be on a s3 bucket and copies are only made whenever the
  content has changed. The utility is able to create/handle siblings sha256 sums
  (or similar).
* [forever.sh](./forever.sh) loops forever doing nothing in its most basic
  behaviour. This can be used to keep containers running without consuming
  resources, once they have performed a backup operation. The script is also
  able to execute commands when started or at each loop to perform healthchecks
  (against external services, for example).

In order to facilitate logic flows, the scripts depend on [wrapper] libraries
that abstracts most [mc] and [s3cmd] operations so they behave more like regular
unix shell utilities.

## Docker

A [Dockerfile](./Dockerfile) adds these scripts and [age] to the regular [mc]
Docker image. To build, run a command similar to the following. If necessary,
you can specify an official minio tag version through the build argument
`MINIO_VERSION` or another [release] of age through `AGE_VERSION`.

```shell
docker build -t s3backup .
```

  [release]: https://github.com/FiloSottile/age/releases

Provided an image tagged `s3backup`, you should be able to use the scripts by
modifying the entrypoint and pointing it to the script to be used.

### Minio

For [mc] to work internally inside the container, you will have to bind mount a
[configuration][config] file. For example, provided a `config.json` file under
the current directory, the following command would print out the help of the
`ifcopy.sh` script:

```shell
docker run -it --rm \
    --entrypoint ifcopy.sh \
    -v $(pwd)/config.json:/root/.mc/config.json \
    s3backup \
        --help
```

For example, the following configuration file would make available the minio
[playground] under `play/` if you had bind-mounted a copy of it onto
`/root/.mc/config.json`.

```json
{
    "version": "10",
    "aliases": {
        "play": {
                "url": "https://play.min.io",
                "accessKey": "Q3AM3UQ867SPQQA43P2F",
                "secretKey": "zuf+tfteSlswRu7BJ86wekitnifILbZam1KYY3TG",
                "api": "S3v4",
                "path": "auto"
        }
    }
}
```

  [config]: https://docs.min.io/docs/minio-client-complete-guide.html#config
  [playground]: https://play.minio.io:9000/minio/

### s3cmd

Similarly, if you wanted to use [s3cmd], you will have to bind mount one or
several configuration files. These arrange will be passed to the `--config`
option of [s3cmd]. The directory `tests/config/` contains examples to run
against the minio [playground]. Unless you change the defaults, these files
should be placed under `/root/.s3cmd` in the container.

In order to trigger the `s3cmd` implementation, you will have to set the
environment variable `S3` to the value `s3cmd`.

## Compatibility

### Backblaze

These utilities are compatible with the new S3-compatibility layer at [b2]. Note
that older buckets are not enabled for S3. At the time of writing, the current
version of `mc` does not seem to support embedding of the bucket name inside the
name of the endpoint host. You will have to create an alias pointing to the root
of all your buckets and use the name of the bucket inside the path to the
locations for backups, etc.

  [b2]: https://www.backblaze.com/b2/docs/s3_compatible_api.html

### GCS

Historically, this project was solely based on [mc]. An implementation on top of
[s3cmd] was added in order to provide a solution to this [bug], which otherwise
impairs the `cleaner.sh` script when running against GCS. The [s3cmd]
implementation tries to provide an API that is almost compatible with original
versions based on [mc].

  [bug]: https://github.com/minio/mc/issues/2398

## Docker-Based Development

### Manual Testing

You do not need to have the [MinIO] [client][mc] installed to run these
utilities. Provided a credentials [configuration][config] file, you can
bind-mount the file into a running container to try these utilities.

Provided you have a `config.json` under the current directory, you should be
able to get a shell prompt with access to the scripts in the directory
`/s3backup` within the container using a command similar to the following one.
Note the double mounting to ensure that the configuration file appears at the
default location for `mc`:

```shell
docker run -it --rm \
    --entrypoint ash \
    -v $(pwd)/config.json:/root/.mc/config.json \
    -v $(pwd):/s3backup \
    minio/mc
```

### Automated Testing

There is an initial series of tests. These are meant to exercise the tools
rather than being proper unit tests. The tests require an installation of Docker
and that you user will be able to run `docker` without elevated privileges. The
tests work against the minio [playground]. They work by creating Docker image
using the project's [Dockerfile](./Dockerfile), creating a container using this
image and running a series of commands within the container and against the
[playground] in a unique bucket.

To run the tests, from the root directory of the project, execute the following
command:

```shell
./tests/test.sh
```

If you want to run the tests against the `s3cmd` implementation instead, set the
environment variable `IMPLEMENTATION` to `s3cmd` instead, e.g.

```shell
IMPLEMENTATION=s3cmd ./tests/test.sh
```

The environment variable `TESTS` can also be used to select less tests to
perform. It should contain a space separated list of one of the following
keywords: `cleaner`, `ifcopy`, `tar`, one for each of the utilities part of this
project.
