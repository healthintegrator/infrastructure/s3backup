#!/usr/bin/env sh

ROOT_DIR=$( cd -P -- "$(dirname -- "$(command -v -- "$0")")" && pwd -P )
LIBDIR=
[ -d "${ROOT_DIR}/lib" ] && LIBDIR="${ROOT_DIR}/lib"
[ -z "$LIBDIR" ] && [ -d "${ROOT_DIR}/../lib" ] && LIBDIR="${ROOT_DIR}/../lib"
[ -z "$LIBDIR" ] && echo "Cannot find lib dir!" >&2 && exit 1

# shell sanity
set -eu

# All (good?) defaults
S3_VERBOSE=${S3_VERBOSE:-0}
S3_TRACE=${S3_TRACE:-0}

FOREVER_MSG=${FOREVER_MSG:-}
FOREVER_BEFORE=${FOREVER_BEFORE:-}
FOREVER_SLEEP=${FOREVER_SLEEP:-15}
FOREVER_FIRST=${FOREVER_FIRST:-}
FOREVER_LOOP=${FOREVER_LOOP:-}
FOREVER_ITERATIONS=${FOREVER_ITERATIONS:-}

# Dynamic vars
cmdname=$(basename "$(readlink -f "$0")")
appname=${cmdname%.*}

# Print usage on stderr and exit
usage() {
  exitcode="$1"
  cat << USAGE >&2

Description:

  $cmdname will possibly execute a command and then sleep forever,
  while printing out a message.

Usage:
  $cmdname [-option arg --long-option(=)arg] [--] command

  where all dash-led options are as follows (long options can be followed by
  an equal sign):
    -m | --message       Message to print at each iteration (default: none)
    -f | --first         First message to print (default: same as --message)
    -b | --before        Command to execute before starting to sleep
    -s | --sleep         Number of seconds to sleep between message output
    -l | --loop          Command to call at each loop (default: none)
    -v | --verbose       Be more verbose
    -h | --help          Print this help and exit.
    -i | --iterations    Will only loop and sleep that many times
USAGE
  exit "$exitcode"
}

module() {
    for module in "$@"; do
        module_path="${LIBDIR}/${module}.sh"
        if [ -f "$module_path" ]; then
            # shellcheck disable=SC1090
            . "$module_path"
        else
            echo "Cannot find module $module at $module_path !" >& 2
            exit 1
        fi
    done
}

# Source in all relevant modules. This is where most of the "stuff" will occur.
module log

# Parse options
while [ $# -gt 0 ]; do
    case "$1" in
        -s | --sleep)
            FOREVER_SLEEP=$2; shift 2;;
        --sleep=*)
            FOREVER_SLEEP="${1#*=}"; shift 1;;

        -m | --message)
            FOREVER_MSG=$2; shift 2;;
        --message=*)
            FOREVER_MSG="${1#*=}"; shift 1;;

        -f | --first)
            FOREVER_FIRST=$2; shift 2;;
        --first=*)
            FOREVER_FIRST="${1#*=}"; shift 1;;

        -b | --before | --prequel)
            FOREVER_BEFORE=$2; shift 2;;
        --before=* | --prequel=*)
            FOREVER_BEFORE="${1#*=}"; shift 1;;

        -l | --loop)
            FOREVER_LOOP=$2; shift 2;;
        --loop=*)
            FOREVER_LOOP="${1#*=}"; shift 1;;

        -i | --limit | --iterations)
            FOREVER_ITERATIONS=$2; shift 2;;
        --limit=* | --iterations=*)
            FOREVER_ITERATIONS="${1#*=}"; shift 1;;

        --trace)
            # shellcheck disable=SC2034
            S3_TRACE=1; shift;;

        -v | --verbose)
            # shellcheck disable=SC2034
            S3_VERBOSE=1; shift;;

        -h | --help)
            usage 0;;
        --)
            shift; break;;
        -*)
            echo "Unknown option: $1 !" >&2 ; usage 1;;
        *)
            break;;
    esac
done


log "*** $appname *** Starting to sleep forever"

# Call command to execute before all sleeping, if present.
if [ -n "$FOREVER_BEFORE" ]; then
    log "Executing command: $FOREVER_BEFORE"
    $FOREVER_BEFORE
fi

if [ -n "$FOREVER_SLEEP" ]; then
    log "Starting to sleep with $FOREVER_SLEEP s. intervals"
    teleprompt=${FOREVER_FIRST:-"$FOREVER_MSG"}
    while true; do
        if [ -n "$FOREVER_ITERATIONS" ] && [ "$FOREVER_ITERATIONS" -le "0" ]; then
            exit
        fi

        # Loop entry message, switching from "first ever" message to "at each loop"
        # message.
        if [ -n "$teleprompt" ]; then
            echo "$teleprompt"
        fi
        teleprompt="$FOREVER_MSG"

        # Execute command at each loop, if relevant.
        if [ -n "$FOREVER_LOOP" ]; then
            log "Executing command: $FOREVER_LOOP"
            $FOREVER_LOOP
        fi

        # Sleep!
        sleep "$FOREVER_SLEEP"

        # Countdown, if relevant
        if [ -n "$FOREVER_ITERATIONS" ]; then
            FOREVER_ITERATIONS=$((FOREVER_ITERATIONS - 1))
        fi
    done
fi