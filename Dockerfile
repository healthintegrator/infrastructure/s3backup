# Minio release. behaviour and arguments have changed between versions, so
# double check compatibility.
ARG MINIO_VERSION=RELEASE.2020-08-08T02-33-58Z
FROM minio/mc:$MINIO_VERSION

# Set age version
ARG AGE_VERSION=1.0.0-beta2
# Install age, so it can be used for client-side file encryption when the remote
# S3 does not support encryption, e.g. when running in (soome?) gateway mode.
RUN wget -q -O /tmp/age.tgz https://github.com/FiloSottile/age/releases/download/v${AGE_VERSION}/age-v${AGE_VERSION}-"$(uname -s | tr '[:upper:]' '[:lower:]')"-amd64.tar.gz && \
    tar x -C /tmp -vf /tmp/age.tgz && \
    mv -f /tmp/age/age* /usr/local/bin && \
    rm -rf /tmp/age*

# Version to use for s3cmd, this is the one which the wrapper was tested
# against.
ARG S3CMD_VERSION=2.1.0
# Install s3cmd
RUN wget -q -O /tmp/s3cmd.tgz https://github.com/s3tools/s3cmd/releases/download/v${S3CMD_VERSION}/s3cmd-${S3CMD_VERSION}.tar.gz && \
    tar x -C /tmp -zvf /tmp/s3cmd.tgz && \
    cd /tmp/s3cmd-${S3CMD_VERSION} && \
    apk add --no-cache python3 py-pip py-setuptools libmagic && \
    pip install python-magic && \
    python3 setup.py install && \
    apk del --no-cache py-pip && \
    cd / && \
    rm -rf /tmp/s3cmd*

COPY lib/*.sh /usr/local/lib/
COPY *.sh /usr/local/bin/

ENTRYPOINT [ "ash" ]
